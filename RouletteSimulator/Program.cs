﻿using System;

namespace RouletteSimulator
{// chapter 5 book
	// todos
	// 1. figure out if the winnings can help the bankroll for max bet  
	// 1.2 do the top for the 2:1 strategy
	// **** 2. the 2:1 max bet isnt right due to the bankroll (ie 40 means 20 twice an 10 twice). 
	// 	// 4. multiple simulations see the history 
	// 5. tests 
    // -- test for bankroll needed for the 2:1 strategy ie did you en on the first 40$ bet or secon 40$ bet
    // -- test if min bet changes does it still work?
	// 6.1 jump between the two strategy
	// 6. james bond stategy ???
	// refactor like crazy 
	//7. dump results to file to examin 
	// 8 . 0 00 half back red black

	class MainClass
	{
		private static Boolean debugger = true; // show the win looses for the hands played 
		public static Random r = new Random ();
		private static int minBet = 10;
		private static int highestBet;
		private static int bankRollNeeded = 0;
		private static int dollarsUp = 0;

		public static void Main (string[] args)
		{
			int run = 1;
			while (run != 0) {
				dollarsUp = 0;
                bankRollNeeded = 0;
				highestBet = minBet;
				displayIntoText ();
				int betStrategy = Convert.ToInt32 (Console.ReadLine ());
				simulate (betStrategy, 99);

				// run again ?
				Console.WriteLine ("0 = break program");
				run = Convert.ToInt32 (Console.ReadLine ());
			}
			Console.ReadLine ();

		}
		public static void simulate(int betStrategy, int rounds)
		{// betstrategy 1 = black red , 2= 2:1 payout or thirds
			if (betStrategy == 1) {
				simulateBlackRedStrategy (rounds);
			}
			else {// its 2 run the other strat
				simulateThirdStrategy (rounds);
			}

		}

		static void simulateBlackRedStrategy (int rounds)
		{
			double wins = 0, losses = 0;
			int currentBet = 10;
			bankRollNeeded = currentBet;
			for (int i = 0; i < rounds; i++) {
				if (currentBet > highestBet) {
					highestBet = currentBet;
					bankRollNeeded += currentBet;
				}
				Boolean win = playRound (1);
				if (win) {
					if (debugger == true) {
						Console.Write("w");
					}
					dollarsUp += currentBet;
					currentBet = minBet;
					wins++;
				}
				else {
					if (debugger == true) {
						Console.Write("l");
					}
					dollarsUp -= currentBet;
					currentBet = currentBet * 2;
					losses++;
				}
			}
			winLossOutput ("Bet on Black/Red Strategy 1:1 payout",wins, losses);
		}

		static void simulateThirdStrategy (int rounds)
		{// 2:1 bet simulate betting thirds
            // solve the bankroll needed problem
			double wins = 0, losses = 0;
			int currentBet = 10;
			int timesLostAlready = 0;
			for (int i = 0; i < rounds; i++) {
				if (currentBet > highestBet) {
					highestBet = currentBet;
                    bankRollNeeded += currentBet;
				}
                else if ((currentBet == highestBet)&&(timesLostAlready==1))
                {
                    bankRollNeeded += currentBet;
                    // AND I AM HITTING it right after i just change my current bet. ie I am not hitting it on
                    // a second run
                    // what if i go to 40$ bet and one the second time i bet 40$ again i win? What if i did 
                    //that a few times this would be wrong ...
                }
				Boolean win = playRound (2);
				if (win) {
					if (debugger == true) {
						Console.Write("w");
					}
					dollarsUp += (currentBet*2);
					currentBet = minBet;
					wins++;
				}
				else {//lost
                    if (currentBet == highestBet)
                    {// i lost the second 
                       // bankRollNeeded += currentBet;// bankroll needed is too high
                    }
					if (debugger == true) {
						Console.Write("l");
					}
					dollarsUp -= currentBet;
					timesLostAlready++;
					if (timesLostAlready > 1) {// i lost a second time in a row on that bet
						currentBet = currentBet * 2;
						timesLostAlready = 0;
					}
					losses++;
				}
			}
			winLossOutput ("Thirds Strategy 2:1 payout",wins, losses);

		}

		static void winLossOutput (String gametype,double wins, double losses)
		{
			Console.WriteLine (" \n");
			Console.WriteLine ("{0}",gametype);
			Console.WriteLine ("~~ The End ~~\n");
			Console.WriteLine (" ");
			Console.WriteLine ("*********************** ");
			Console.WriteLine ("* wins:" + wins + " losses:" + losses + " *");
			Console.WriteLine ("*********************** ");
			Console.WriteLine (" ");
			Console.WriteLine ((wins / losses) + "=your win/loss ratio \n");
			Console.WriteLine ("Your earnings " + dollarsUp + "$");
			Console.WriteLine ("Highest Bet=" + highestBet + "$");
			Console.WriteLine ("Bankroll Needed=" + bankRollNeeded + "$");
			Console.WriteLine ("___________________________ \n ");
		}

		public static Boolean playRound (int stategy)
		{
			Boolean iWon;
			if(stategy==1)
			{// they are doing odd / even i assume odd to make the 0,00 count against them
				int number = Convert.ToInt32 (spin ());
				if ((number % 2) == 1) {/// 0 % 2 = 0
					iWon=true;
				} else {
					iWon=false;
				}
			}
			else{// strategy is 2 so they are doing 2:1 payout I do the middle row 2,5,8...35 
				int number = Convert.ToInt32 (spin ());
				if ((number % 3) == 2) {
					iWon=true;
				} else {
					iWon=false;
				}
			}
			return iWon;
		}
		public static String spin()
		{
			int ballNum = r.Next(0, 38);
			String sBallNum = ballNum.ToString ();
			if (ballNum == 37) {// then its 00 
				sBallNum = "00";
			} 
			return sBallNum;
		}
		public static void displayIntoText()
		{
			Console.WriteLine("!~~~~  Roulette Simulator  ~~~~!\n");
			Console.WriteLine("10$ = min bet");

			Console.WriteLine ("choose your betting strategy");
			Console.WriteLine ("1. Red/Black or Odd/Even aka 1:1 payout");
			Console.WriteLine ("2. 1/3 betting aka 2:1 payout");
		}

	}
}